import React from "react";
import ReactDOM from "react-dom";
import Countdown from "./Countdown.js";
import { Wrapper } from './Styling';

ReactDOM.render(
    <>
        <Wrapper/>
        <Countdown targetDate={"06-30-2022"}/>
    </>
    , document.getElementById("root"));