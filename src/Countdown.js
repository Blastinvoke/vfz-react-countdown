import React from "react";
import moment, { duration } from 'moment';
import { useEffect, useState } from 'react';
import { Panel, Title, Timer, TimerSegment, TimerSegmentNumber, TimerSegmentCaption, FinishedMessage} from './Styling';

const Countdown = ({targetDate}) => {
  const countdownTitle  = "iPhone 100.";
  const launchedMessage = "Launched!";

  const [finished, setFinished] = useState(false);
  const [timer, setTimer] = useState({
    days:     0,
    hours:    0,
    minutes:  0,
    seconds:  0
  }); 

  useEffect(() => {
    const interval = setInterval(() => {
      updateTimer(targetDate);
    }, 1000);
    
    return () => clearInterval(interval);
  }, [targetDate]);

  const updateTimer = (targetDate) => {
    const formatted_target_date = moment(targetDate);
    const now                   = moment();

    const remainingTime         = duration(formatted_target_date.diff(now));
    
    if(formatted_target_date.isBefore(now))
      setFinished(true);
    else
      setTimer({
        days:     Math.floor(remainingTime.asDays()),
        hours:    remainingTime.hours(),
        minutes:  remainingTime.minutes(),
        seconds:  remainingTime.seconds() 
      });
  }

  const determineCaption = (timer_segment) => {
    switch(timer_segment) {
      case 'days':
        return (timer[timer_segment] === 1 ? 'day' : timer_segment);
      case 'hours':
        return (timer[timer_segment] === 1 ? 'hour' : timer_segment);
      case 'minutes':
        return (timer[timer_segment] === 1 ? 'minutes' : timer_segment);
      case 'seconds':
        return (timer[timer_segment] === 1 ? 'second': timer_segment);
      default:
        return '';
    }
  }

  return (
      <Panel data-testid="panel-render">
        <Title>{countdownTitle}</Title>
        <Timer>
          {finished ?
            <FinishedMessage>{launchedMessage}</FinishedMessage>
          :
            <>
              {Object.keys(timer).map((timer_segment, index) => {
                return (
                  <TimerSegment key={index}>
                    <TimerSegmentNumber>{timer[timer_segment]}</TimerSegmentNumber>
                    <TimerSegmentCaption>{determineCaption(timer_segment)}</TimerSegmentCaption>
                  </TimerSegment>
                );
              })}
            </>
          }
        </Timer>
      </Panel>
  );
};

export default Countdown;
