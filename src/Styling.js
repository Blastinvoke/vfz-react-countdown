import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components'
import Background from './assets/Background.png';

export const Wrapper = createGlobalStyle`
    html, body {
        background-image: url(${Background});
        background-size: 100% 100%;
        display: flex;
        flex: 1;
        height: 100%;
        align-items: center;
        justify-content: center;
    }
`;

export const Panel = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
`;

export const Title = styled.p`
    display: flex;
    flex: 1;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 80px;
`;

export const Timer = styled.div`
    display: flex;
    flex: 1;
    flex-direction: row;
    justify-content: space-around;
`;

export const TimerSegment = styled.div`
    display: flex;
    margin-left: 28px;
    margin-right: 28px; 
    align-items: center;
    flex-direction: column;
`;

export const TimerSegmentNumber = styled.p`
    display: flex;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 80px;
`;

export const TimerSegmentCaption = styled.p`
    display: flex;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 20px;
`;

export const FinishedMessage = styled.p`
    display: flex;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 50px;
`;