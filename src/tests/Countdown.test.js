import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Countdown from '../Countdown';

describe("Countdown tests", () => {
  render(<Countdown targetDate={"12-31-9999"}/>);

  it("Should render countdown panel correctly", () => {
    const panel = screen.getByTestId('panel-render')
    expect(panel).toBeInTheDocument();
  });
});
